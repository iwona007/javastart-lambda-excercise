package iwona.pl.model;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.junit.Test;

public class Functional {

    @Test
    public void funcion() {
//        R apply(T t); Integer p -> String
        Function<Integer, String> function = t -> t + t + " napis";
        System.out.println(function.apply(70));
    }

    @Test
    public void predicate() {
//        boolean test(T t);  T t ->
        Predicate<Student> predicate = student -> student.getAge() > 20;

        boolean test1 = predicate.test(new Student("Alina", "Nowak", 19));
        boolean test2 = predicate.test(new Student("Michał", "Nowak", 30));

        System.out.println(test1);
        System.out.println(test2);
    }

    @Test
    public void classicPrograming() {
        List<String> names = Arrays.asList("Jan", "Karol", "Piotr", "Andrzej");
        long counter = 0;
        for (String name : names) {
            if (name.length() > 5) {
                counter++;
            }
        }
        System.out.println(counter);
    }

    @Test
    public void functionalPrograming(){
        List<String> names = Arrays.asList("Jan", "Karol", "Piotr", "Andrzej");
        long count = names.stream().filter(name -> name.length() > 5)
                .count();
        System.out.println(count);
    }

    @Test
    public void mapMethod(){
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

        List<Integer> collect = numbers.stream().map(number -> number * 2)
                .collect(Collectors.toList());
        System.out.println(numbers);
        System.out.println(collect);
    }

}
