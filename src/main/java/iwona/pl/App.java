package iwona.pl;

import iwona.pl.classicalSolution.Exercise;
import iwona.pl.lambdaSolution.*;
import iwona.pl.model.Person;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Supplier;

public class App {
    public static void main(String[] args) {
        Exercise exercise = new Exercise();
        System.out.println("Transformaacja String'a za pomocą pętli");
        System.out.println(exercise.getToLowerCaseTrim("   WIELKI NAPIS   "));
        System.out.println("\n"+"Transformaacja String'a za pomocą lambdy");
        System.out.println(LambdaEx.getToLowerCaseTrim());

        System.out.println("\n"+"Drukowanie String'a za pomocą pętli");
        exercise.print("abc");
//        lambda
        System.out.println("\n"+"Drukowanie String'a za pomocą lambdy");
        LambdaEx.print("xyz");

        System.out.println("\n"+"Filtrowanie wieku za pomocą pętli");
        exercise.age();
//        lambda
        System.out.println("\n"+"Filtrowanie wieku za pomocą lambdy");
        LambdaEx.age();

        System.out.println("\n"+"Tablica Person za pomocą pętli");
        exercise.tableOfPeople();
        System.out.println("\n"+"Tablica Person za pomocą lambdy");
        LambdaEx.tableOfPeople();


        //Interfejsy Funkcyjne
        ConsumerEx consumerEx = new ConsumerEx();
        consumerEx.createPerson();
        PredicateFilter predicateFilter = new PredicateFilter();
        FunctionMap functionMap = new FunctionMap();
        SupplierExample supplierExample = new SupplierExample();
        System.out.println("\n"+"Zmiana wieku pętla;");
        consumerEx.print();
        System.out.println("\n"+"Add age");
        consumerEx.addAge();
        System.out.println("\n"+"After Added");
        consumerEx.print();
        System.out.println("\n" + "Zmiana wieku z wykorzystaniem lambdy i interfejsu Consumer" + "\n");
// lambda
        consumerEx.consumeList(consumerEx.people, p -> p.setAge(p.getAge() + 5));
        consumerEx.consumeList(consumerEx.people, p -> System.out.println(p));
        System.out.println();
        System.out.println("Filtrowanie wieku - interfajs Predicate ");
        List<Person> adults = predicateFilter.filterByPredicate(consumerEx.people, p -> p.getAge() >= 18);
        consumerEx.consumeList(adults, p -> System.out.println(p));
        System.out.println();
        System.out.println("Filtrowanie po imieniu boolean test(T t);  T t -> boolean - interfajs Predicate ");
        List<Person> janPeople = predicateFilter.filterByPredicate(consumerEx.people, p -> "Jan".equals(p.getFirstName()));
        consumerEx.consumeList(janPeople, p -> System.out.println(p));

        System.out.println();
        System.out.println("Mapowanie interfejs Function zamiana listy Person na listę imion String");
        List<String> result = functionMap.convertList(consumerEx.people, person -> person.getFirstName());
        consumerEx.consumeList(result, p -> System.out.println(p));
        System.out.println();
        System.out.println("Mapowanie interfejs Function zamiana listy Person na listę z wiekiem ");
        List<Integer> ages = functionMap.convertList(consumerEx.people, person -> person.getAge());
        consumerEx.consumeList(ages, p -> System.out.println(p));


//Supplier
            String[] firstNames = {"Jan", "Karol", "Piotr", "Andrzej"};
            String[] lastNames = {"Abacki", "Kowalski", "Zalewski", "Korzeniewski"};
            int[] ages2 = {22, 33, 44, 55};
            Random random = new Random();

            Supplier<Person> supplier = () -> {
                String firstName = firstNames[random.nextInt(firstNames.length)];
                String lastName = lastNames[random.nextInt(lastNames.length)];
                int age = ages2[random.nextInt(ages2.length)];
                return new Person(firstName, lastName, age);
            };

            List<Person> people = new ArrayList<>();
            for (int i = 0; i < 5; i++) {
                people.add(supplier.get());
            }

            for (Person person : people) {
                System.out.println(person);
            }

        System.out.println("Stworzenie obiekty interface Supplier ");
        List<Person> people2 = supplierExample.generateRandomList(5, supplier);
        consumerEx.consumeList(people2, p -> System.out.println(p));
    }
}
