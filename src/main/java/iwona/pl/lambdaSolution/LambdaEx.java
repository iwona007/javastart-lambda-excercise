package iwona.pl.lambdaSolution;

import iwona.pl.model.Person;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class LambdaEx {

    // Function<T, R> -   R apply(T t);
    public static String getToLowerCaseTrim() {
        Function<String, String> func = (String s) -> s.toLowerCase().trim();
//        lub
        Function<String, String> func2 = text -> text.toLowerCase().trim();

        String word = "   WIELKI NAPIS   ";
        String lowerCaseTrim = func.apply(word);
        return lowerCaseTrim;
    }

    //    Consumer<T>  accept(T t)
    public static void print(String word) {
        Consumer<String> print = s -> {
            System.out.println(s);
            System.out.println(s);
            System.out.println(s);
        };
        print.accept(word);
    }

    //   Predicate<T>   test(T t)
    public static void age() {
        int age1 = 16;
        Predicate<Integer> isAdult = age -> age >= 18;
        if (isAdult.test(age1)) {
            System.out.println("is Adult");
        } else {
            System.out.println("not Adult");
        }
    }


    //    Supplier<T>  get()  () -> Person.
    public static void tableOfPeople() {
        String[] firstNames = {"Jan", "Karol", "Piotr", "Andrzej"};
        String[] lastNames = {"Abacki", "Kowalski", "Zalewski", "Korzeniewski"};
        int[] ages = {22, 33, 44, 55};
        Random random = new Random();

        Supplier<Person> supplier = () -> {
            String firstName = firstNames[random.nextInt(firstNames.length)];
            String lastName = lastNames[random.nextInt(lastNames.length)];
            int age = ages[random.nextInt(ages.length)];
            return new Person(firstName, lastName, age);
        };

        List<Person> people = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            people.add(supplier.get());
        }

        for(Person person: people){
            System.out.println(person);
        }

    }
}
