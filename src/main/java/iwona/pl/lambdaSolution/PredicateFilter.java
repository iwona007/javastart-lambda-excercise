package iwona.pl.lambdaSolution;

import iwona.pl.model.Person;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class PredicateFilter {

    public List<Person> filterAdults(List<Person> people) {
        List<Person> adults = new ArrayList<>();
        for (Person person : people) {
            if (person.getAge() >= 18) { //Person p -> boolean
                adults.add(person);
            }
        }
        return adults;
    }

    public List<Person> filterJan(List<Person> people) {
        List<Person> janPeople = new ArrayList<>();
        for (Person person : people) {
            if ("Jan".equals(person.getFirstName())) { //Person p -> boolean
                janPeople.add(person);
            }
        }
        return janPeople;
    }


    // boolean test(T t);  T t -> boolean
    public <T> List<T> filterByPredicate(List<T> list, Predicate<T> predicate) {
        List<T> result = new ArrayList<>();
        for (T t : list) {
            if (predicate.test(t)) {
                result.add(t);
            }
        }
        return result;
    }

}
