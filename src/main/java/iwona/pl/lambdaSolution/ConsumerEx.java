package iwona.pl.lambdaSolution;

import iwona.pl.model.Person;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class ConsumerEx {

    public List<Person> people;

    public List<Person> createPerson() {
        this.people = new ArrayList<>();
        people.add(new Person("Jan", "Kowalski", 42));
        people.add(new Person("Kasia", "Kruczkowska", 22));
        people.add(new Person("Piotr", "Adamiak", 15));
        people.add(new Person("Jan", "Zawadzki", 17));
        people.add(new Person("Krzysztof", "Wojtyniak", 16));
        people.add(new Person("Agnieszka", "Zagumna", 18));
        people.add(new Person("Basia", "Cyniczna", 28));
        return people;
    }

    public void print() {
        for (Person person : people) {
            System.out.println(person); //Person person -> void
        }
    }

    public void addAge() {
        for (Person person : people) {
            int currentAge = person.getAge();
            person.setAge(currentAge + 1); //Person person -> void
        }
    }

    //    void accept(T t);
    public <T> void consumeList(List<T> list, Consumer<T> consumer) {
        for (T t : list) {
            consumer.accept(t);
        }
    }


    //    Predicate


}
