package iwona.pl.lambdaSolution;

import iwona.pl.model.Person;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class FunctionMap {

    public List<String> printName(List<Person> list) {
        List<String> firstNames = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            Person person = list.get(i);
            String firstName = person.getFirstName(); // Person p -> String
            firstNames.add(firstName);
        }
//        lub
//        for (Person person : list) {
//            firstNames.add(person.getFirstName());
//        }
        return firstNames;
    }

//    R apply(T t);

    public <T, R> List<R> convertList(List<T> list, Function<T, R> function) {
        List<R> resultList = new ArrayList<>();
        for (T t : list) {
            R result = function.apply(t); //String result = person.getFirstName()
            resultList.add(result);
        }
        return resultList;
    }
}
