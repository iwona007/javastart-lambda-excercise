package iwona.pl.classicalSolution;

import iwona.pl.model.Person;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Exercise {

    public String getToLowerCaseTrim(String word) {
        return word.toLowerCase().trim();
    }

    public void print(String abc) {
        System.out.println(abc);
        System.out.println(abc);
        System.out.println(abc);
    }


    public void age() {
        if (isAdult(20)) {
            System.out.println("Is adult");
        } else {
            System.out.println("Is not adult");
        }

    }

    private static boolean isAdult(int age) {
        return age >= 18;
    }


    public List<Person> tableOfPeople() {
        Random random = new Random();
        String[] firstNames = {"Jan", "Karol", "Piotr", "Andrzej"};
        String[] lastNames = {"Abacki", "Kowalski", "Zalewski", "Korzeniewski"};
        int[] ages = {22, 33, 44, 55};

        List<Person> people = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            String firstName = firstNames[random.nextInt(firstNames.length)];
            String lastName = lastNames[random.nextInt(lastNames.length)];
            int age = ages[random.nextInt(ages.length)];
            Person randomPerson = new Person(firstName, lastName, age);
            people.add(randomPerson);
        }
        for (Person person : people) {
            System.out.println(person);
        }
        return people;
    }


}

